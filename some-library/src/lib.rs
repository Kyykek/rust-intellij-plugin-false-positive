pub struct GenericType<T>(T);

pub trait BaseTrait {}

// `T: BaseTrait` implies `GenericType<T>: BaseTrait`
impl<T: BaseTrait> BaseTrait for GenericType<T> {}
