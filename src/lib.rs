use some_library::{BaseTrait, GenericType};

pub trait BoundTrait: BaseTrait {}

// the trait bound `GenericType<T>: BaseTrait` is not satisfied [E0277]
// the trait `BaseTrait` is not implemented for `GenericType<T>`
impl<T: BoundTrait> BoundTrait for GenericType<T> {}